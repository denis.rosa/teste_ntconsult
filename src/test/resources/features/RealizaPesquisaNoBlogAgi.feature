#language: pt
#encoding: utf-8
@all
Funcionalidade: Valida as possibilidades de Pesquisa no Blog do Agibank

  Contexto:
    Dado que acesso o site do Blog do Agibank

  @pesquisa
  Cenario: [Pesquisa] - Realiza Pesquisa no Blog Agi com sucesso
    Quando realizo uma pesquisa por "Carreira"
    Entao a pesquisa deve retornar resultados

  @pesquisa
  Cenario: [Pesquisa] - Realiza Pesquisa no Blog Agi com sucesso sem preencher o campo de pesquisa
    Quando realizo uma pesquisa por ""
    Entao a pesquisa deve retornar resultados

  @pesquisa
  Cenario: [Pesquisa] - Realiza Pesquisa no Blog Agi preenchendo o campo de pesquisa com valor numérico
    Quando realizo uma pesquisa por "78653476325467"
    Entao a pesquisa não deve retornar resultados

  @pesquisa
  Cenario: [Pesquisa] - Realiza Pesquisa no Blog Agi preenchendo o campo de pesquisa com caracteres especiais
    Quando realizo uma pesquisa por "@$#%@%$#"
    Entao a pesquisa não deve retornar resultados