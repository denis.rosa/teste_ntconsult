package pageElements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.CommonPage;

import java.util.List;

public class BlogDoAgiResultadoPesquisaPageElementMap extends CommonPage {

    @FindBy(css = "[class='post-content ast-grid-common-col']")
    protected List<WebElement> resultadoPesquisa;

    @FindBy(xpath = "//*[contains(text(),'Lamentamos, mas nada foi encontrado para sua pesquisa, tente novamente com outras palavras.')]")
    protected WebElement semResultadoNaPesquisa;

}
