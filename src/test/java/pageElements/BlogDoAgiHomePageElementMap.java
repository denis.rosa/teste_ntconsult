package pageElements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.CommonPage;

import java.util.List;

public class BlogDoAgiHomePageElementMap extends CommonPage {

    @FindBy(css = "[class='ast-search-menu-icon slide-search']")
    protected WebElement lupaPesquisar;

    @FindBy(css = "[id='search-field']")
    protected WebElement inputPesquisar;

    @FindBy(css = "[class='post-content ast-grid-common-col']")
    protected List<WebElement> resultadoPesquisa;

    @FindBy(css = "[name='ed20d946']")
    protected WebElement inputNome;

    @FindBy(css = "[type='email']")
    protected WebElement inputEmail;

    @FindBy(css = "[class='uagb-forms-main-submit-button wp-block-button__link']")
    protected WebElement btnEnviar;

    @FindBy(xpath = "//*[contains(text(),'Se inscreva para receber as principais novidades do Blog do Agi! ')]")
    protected WebElement msgSucessoSeInscreva;

    @FindBy(xpath = "//*[contains(text(),'Obrigado! Agora você receberá todas as novidades por e-mail. ')]")
    protected WebElement msgSucessoObrigado;



}
