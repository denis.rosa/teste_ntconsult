package steps;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import org.junit.Assert;
import pages.BlogDoAgiHomePage;
import util.Utils;

public class blogDoAgiHomeSteps {

    Utils util = new Utils();

    @E("realizo uma pesquisa por \"(.*)\"")
    public void pesquisarNoBlog(String pesquisa){
        BlogDoAgiHomePage blogDoAgiHomePage = new BlogDoAgiHomePage();
        blogDoAgiHomePage.pesquisarProduto(pesquisa);
        util.logPrint("Home do Stack Overflow - Realizando pesquisa:" + pesquisa);
    }

    @E("preencho o formulario com nome \"(.*)\" e email \"(.*)\" para receber novidades")
    public void preencheOFormulario(String nome, String email){
        BlogDoAgiHomePage blogDoAgiHomePage = new BlogDoAgiHomePage();
        blogDoAgiHomePage.preencheOFormulario(nome, email);
        util.logPrint("Formulario de Novidades:");
    }

    @Entao("Valido que as mensagens de sucesso foram apresentadas")
    public void validaMensagensDeSucesso(){
        BlogDoAgiHomePage blogDoAgiHomePage = new BlogDoAgiHomePage();
        Assert.assertTrue("A mensagens não foram apresentadas, Verifique!.", blogDoAgiHomePage.validaSeMensagensDeSucessoForamExibidas());
        util.logPrint("Tela de Resultado da Busca: ");
    }
}
