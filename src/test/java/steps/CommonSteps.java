package steps;


import cucumber.api.java.pt.Dado;
import util.TestRule;
import util.Utils;

public class CommonSteps {

    Utils util = new Utils();

    @Dado("que acesso o site do Blog do Agibank")
    public void acessarSiteDoBlogDoAgi(){
        String blogAgi = "https://blogdoagi.com.br/";
        TestRule.abrirNavegador(blogAgi);
        util.logPrint("Home do Blog do Agibank");
    }

}
