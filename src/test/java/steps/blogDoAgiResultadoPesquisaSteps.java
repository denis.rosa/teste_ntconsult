package steps;

import cucumber.api.java.pt.Entao;
import org.junit.Assert;
import pages.BlogDoAgiResultadoPesquisaPage;
import util.Utils;

public class blogDoAgiResultadoPesquisaSteps {

    Utils util = new Utils();

    @Entao("a pesquisa deve retornar resultados")
    public void validarQuePesquisaRetornouResultados(){
        BlogDoAgiResultadoPesquisaPage blogDoAgiResultadoPesquisaPage = new BlogDoAgiResultadoPesquisaPage();
        util.logPrint("Tela de Resultado da Busca: ");
        Assert.assertTrue("O resultado da pesquisa está vazio.", blogDoAgiResultadoPesquisaPage.isPesquisaRetornouResultados());
    }

    @Entao("a pesquisa não deve retornar resultados")
    public void validarQuePesquisaNaoRetornouResultados(){
        BlogDoAgiResultadoPesquisaPage blogDoAgiResultadoPesquisaPage = new BlogDoAgiResultadoPesquisaPage();
        util.logPrint("Tela de Resultado da Busca: ");
        Assert.assertTrue("O resultado da pesquisa não está vazio.", blogDoAgiResultadoPesquisaPage.isPesquisaNaoTeveResultados());
    }
}
