package pages;

import org.openqa.selenium.support.PageFactory;
import pageElements.BlogDoAgiResultadoPesquisaPageElementMap;
import util.Utils;

public class BlogDoAgiResultadoPesquisaPage extends BlogDoAgiResultadoPesquisaPageElementMap {

    Utils util = new Utils();

    public BlogDoAgiResultadoPesquisaPage(){
        PageFactory.initElements(driver, this);
    }

    public boolean isPesquisaRetornouResultados(){
        boolean ok = false;
        if (resultadoPesquisa.size() > 0){
            ok = true;
        }
        return ok;
    }

    public boolean isPesquisaNaoTeveResultados(){
        waitVisibilityOf(semResultadoNaPesquisa, 3);
        boolean ok = false;
        if (resultadoPesquisa.size() == 0 && semResultadoNaPesquisa.isDisplayed()){
            ok = true;

        }
        return ok;
    }

}
