package pages;

import org.openqa.selenium.support.PageFactory;
import pageElements.BlogDoAgiHomePageElementMap;
import util.Utils;

public class BlogDoAgiHomePage extends BlogDoAgiHomePageElementMap {

    Utils util = new Utils();

    public BlogDoAgiHomePage(){
        PageFactory.initElements(driver, this);
    }

    public void pesquisarProduto(String pesquisa){
        waitVisibilityOf(lupaPesquisar,3);
        lupaPesquisar.click();
        waitVisibilityOf(inputPesquisar, 3);
        inputPesquisar.sendKeys(pesquisa);
        inputPesquisar.submit();
    }

    public void preencheOFormulario(String nome, String email){
        waitVisibilityOf(lupaPesquisar, 5);
        inputNome.sendKeys(nome);
        inputEmail.sendKeys(email);
        btnEnviar.click();
    }

    public Boolean validaSeMensagensDeSucessoForamExibidas(){
            boolean ok = false;
            util.sleep(7000);
            if (msgSucessoSeInscreva.isDisplayed() && msgSucessoObrigado.isDisplayed()){
                ok = true;
            }
            return ok;

    }

}
