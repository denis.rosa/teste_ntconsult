package util;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;

import java.io.File;
import java.io.IOException;

public class Utils{

    private static WebDriver driver;
    protected static void setDriverByOS(){
        String driverPath = "src/test/resources/drivers/";
        if(System.getProperty("os.name").contains("Windows")){
            driverPath = driverPath + "chromedriver.exe";
        }else{
            driverPath = driverPath + "chromedriver";
        }
        System.setProperty("webdriver.chrome.driver", driverPath);
    }

    public static void sleep(int tempo){
        try {
            Thread.sleep(tempo);
        }catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static void logPrint(String log){
        ExtentTest extentTest = TestRule.getExtentTest();
        try {
            efetuarPrintTela(log);
            extentTest.log(Status.INFO, log, MediaEntityBuilder.createScreenCaptureFromPath(
                    System.getProperty("user.dir") + "/Report/ExtentReport/Prints/" + log + ".png").build());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void efetuarPrintTela(String log){
        File srcFile = ((TakesScreenshot) TestRule.getDriver()).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(srcFile,
                    new File(System.getProperty("user.dir") + "/Report/ExtentReport/Prints/" + log +".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void realizaScrollToElement(WebElement element){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", element);
    }

}
