package util;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class TestRule extends TestWatcher {

    private static ExtentHtmlReporter htmlReporter;
    private static ExtentReports extentReport;
    private static ExtentTest extentTest;
    private static WebDriver driver;

    public TestRule(){
        super();
    }

    @Override
    protected void starting(Description description){}

    @Before
    public void beforeScenario(Scenario scenario){
        if(extentReport == null){
            extentReport = new ExtentReports();
            htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/Report/ExtentReport/htmlReporter.html");
            extentReport.attachReporter(htmlReporter);
        }
        extentTest = extentReport.createTest(scenario.getId());
    }

    @After
    public void afterScenario(Scenario scenario){
        if(driver != null){
            driver.close();
            driver.quit();
            driver = null;
        }
        extentTest.log(Status.PASS, "Cenário: " + scenario.getName() + " executado com sucesso!");
        extentReport.flush();
    }

    public static ExtentTest getExtentTest(){
        return extentTest;
    }

    protected void finished(Description description){
        super.finished(description);
    }

    public static WebDriver getDriver(){
        return driver;
    }

    public static void abrirNavegador(String url){
        ChromeOptions chromeOpts = new ChromeOptions();
        chromeOpts.addArguments("start-maximized");
        Utils.setDriverByOS();

        driver = new ChromeDriver(chromeOpts);
        driver.manage().window().maximize();
        driver.navigate().to(url);
    }

}
