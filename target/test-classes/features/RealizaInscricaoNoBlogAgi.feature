#language: pt
#encoding: utf-8
@all
Funcionalidade: Valida a Funcionalidade de Inscrever para Receber Novidades

  Contexto:
    Dado que acesso o site do Blog do Agibank

  @pesquisa
  Cenario: [Inscrição para receber Novidades] - Realiza inscrição no Blog Agi com sucesso
    Quando preencho o formulario com nome "Joao" e email "joao@teste.com.br" para receber novidades
    Entao Valido que as mensagens de sucesso foram apresentadas
