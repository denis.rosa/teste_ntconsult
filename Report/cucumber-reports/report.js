$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/RealizaInscricaoNoBlogAgi.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#language: pt"
    },
    {
      "line": 2,
      "value": "#encoding: utf-8"
    }
  ],
  "line": 4,
  "name": "Valida a Funcionalidade de Inscrever para Receber Novidades",
  "description": "",
  "id": "valida-a-funcionalidade-de-inscrever-para-receber-novidades",
  "keyword": "Funcionalidade",
  "tags": [
    {
      "line": 3,
      "name": "@all"
    }
  ]
});
formatter.before({
  "duration": 94437417,
  "status": "passed"
});
formatter.background({
  "line": 6,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Contexto"
});
formatter.step({
  "line": 7,
  "name": "que acesso o site do Blog do Agibank",
  "keyword": "Dado "
});
formatter.match({
  "location": "CommonSteps.acessarSiteDoBlogDoAgi()"
});
formatter.result({
  "duration": 3277820375,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "[Inscrição para receber Novidades] - Realiza inscrição no Blog Agi com sucesso",
  "description": "",
  "id": "valida-a-funcionalidade-de-inscrever-para-receber-novidades;[inscrição-para-receber-novidades]---realiza-inscrição-no-blog-agi-com-sucesso",
  "type": "scenario",
  "keyword": "Cenario",
  "tags": [
    {
      "line": 9,
      "name": "@pesquisa"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "preencho o formulario com nome \"Joao\" e email \"joao@teste.com.br\" para receber novidades",
  "keyword": "Quando "
});
formatter.step({
  "line": 12,
  "name": "Valido que as mensagens de sucesso foram apresentadas",
  "keyword": "Entao "
});
formatter.match({
  "arguments": [
    {
      "val": "Joao",
      "offset": 32
    },
    {
      "val": "joao@teste.com.br",
      "offset": 47
    }
  ],
  "location": "blogDoAgiHomeSteps.preencheOFormulario(String,String)"
});
formatter.result({
  "duration": 446017584,
  "status": "passed"
});
formatter.match({
  "location": "blogDoAgiHomeSteps.validaMensagensDeSucesso()"
});
formatter.result({
  "duration": 7225006041,
  "status": "passed"
});
formatter.after({
  "duration": 261460750,
  "status": "passed"
});
formatter.uri("features/RealizaPesquisaNoBlogAgi.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#language: pt"
    },
    {
      "line": 2,
      "value": "#encoding: utf-8"
    }
  ],
  "line": 4,
  "name": "Valida as possibilidades de Pesquisa no Blog do Agibank",
  "description": "",
  "id": "valida-as-possibilidades-de-pesquisa-no-blog-do-agibank",
  "keyword": "Funcionalidade",
  "tags": [
    {
      "line": 3,
      "name": "@all"
    }
  ]
});
formatter.before({
  "duration": 43167,
  "status": "passed"
});
formatter.background({
  "line": 6,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Contexto"
});
formatter.step({
  "line": 7,
  "name": "que acesso o site do Blog do Agibank",
  "keyword": "Dado "
});
formatter.match({
  "location": "CommonSteps.acessarSiteDoBlogDoAgi()"
});
formatter.result({
  "duration": 1805768625,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "[Pesquisa] - Realiza Pesquisa no Blog Agi com sucesso",
  "description": "",
  "id": "valida-as-possibilidades-de-pesquisa-no-blog-do-agibank;[pesquisa]---realiza-pesquisa-no-blog-agi-com-sucesso",
  "type": "scenario",
  "keyword": "Cenario",
  "tags": [
    {
      "line": 9,
      "name": "@pesquisa"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "realizo uma pesquisa por \"Carreira\"",
  "keyword": "Quando "
});
formatter.step({
  "line": 12,
  "name": "a pesquisa deve retornar resultados",
  "keyword": "Entao "
});
formatter.match({
  "arguments": [
    {
      "val": "Carreira",
      "offset": 26
    }
  ],
  "location": "blogDoAgiHomeSteps.pesquisarNoBlog(String)"
});
formatter.result({
  "duration": 2432537166,
  "status": "passed"
});
formatter.match({
  "location": "blogDoAgiResultadoPesquisaSteps.validarQuePesquisaRetornouResultados()"
});
formatter.result({
  "duration": 271551458,
  "status": "passed"
});
formatter.after({
  "duration": 206305916,
  "status": "passed"
});
formatter.before({
  "duration": 43333,
  "status": "passed"
});
formatter.background({
  "line": 6,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Contexto"
});
formatter.step({
  "line": 7,
  "name": "que acesso o site do Blog do Agibank",
  "keyword": "Dado "
});
formatter.match({
  "location": "CommonSteps.acessarSiteDoBlogDoAgi()"
});
formatter.result({
  "duration": 1817687625,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "[Pesquisa] - Realiza Pesquisa no Blog Agi com sucesso sem preencher o campo de pesquisa",
  "description": "",
  "id": "valida-as-possibilidades-de-pesquisa-no-blog-do-agibank;[pesquisa]---realiza-pesquisa-no-blog-agi-com-sucesso-sem-preencher-o-campo-de-pesquisa",
  "type": "scenario",
  "keyword": "Cenario",
  "tags": [
    {
      "line": 14,
      "name": "@pesquisa"
    }
  ]
});
formatter.step({
  "line": 16,
  "name": "realizo uma pesquisa por \"\"",
  "keyword": "Quando "
});
formatter.step({
  "line": 17,
  "name": "a pesquisa deve retornar resultados",
  "keyword": "Entao "
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 26
    }
  ],
  "location": "blogDoAgiHomeSteps.pesquisarNoBlog(String)"
});
formatter.result({
  "duration": 2135276500,
  "status": "passed"
});
formatter.match({
  "location": "blogDoAgiResultadoPesquisaSteps.validarQuePesquisaRetornouResultados()"
});
formatter.result({
  "duration": 302708833,
  "status": "passed"
});
formatter.after({
  "duration": 189114833,
  "status": "passed"
});
formatter.before({
  "duration": 41584,
  "status": "passed"
});
formatter.background({
  "line": 6,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Contexto"
});
formatter.step({
  "line": 7,
  "name": "que acesso o site do Blog do Agibank",
  "keyword": "Dado "
});
formatter.match({
  "location": "CommonSteps.acessarSiteDoBlogDoAgi()"
});
formatter.result({
  "duration": 2029905667,
  "status": "passed"
});
formatter.scenario({
  "line": 20,
  "name": "[Pesquisa] - Realiza Pesquisa no Blog Agi preenchendo o campo de pesquisa com valor numérico",
  "description": "",
  "id": "valida-as-possibilidades-de-pesquisa-no-blog-do-agibank;[pesquisa]---realiza-pesquisa-no-blog-agi-preenchendo-o-campo-de-pesquisa-com-valor-numérico",
  "type": "scenario",
  "keyword": "Cenario",
  "tags": [
    {
      "line": 19,
      "name": "@pesquisa"
    }
  ]
});
formatter.step({
  "line": 21,
  "name": "realizo uma pesquisa por \"78653476325467\"",
  "keyword": "Quando "
});
formatter.step({
  "line": 22,
  "name": "a pesquisa não deve retornar resultados",
  "keyword": "Entao "
});
formatter.match({
  "arguments": [
    {
      "val": "78653476325467",
      "offset": 26
    }
  ],
  "location": "blogDoAgiHomeSteps.pesquisarNoBlog(String)"
});
formatter.result({
  "duration": 1935318875,
  "status": "passed"
});
formatter.match({
  "location": "blogDoAgiResultadoPesquisaSteps.validarQuePesquisaNaoRetornouResultados()"
});
formatter.result({
  "duration": 246882083,
  "status": "passed"
});
formatter.after({
  "duration": 189226917,
  "status": "passed"
});
formatter.before({
  "duration": 41792,
  "status": "passed"
});
formatter.background({
  "line": 6,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Contexto"
});
formatter.step({
  "line": 7,
  "name": "que acesso o site do Blog do Agibank",
  "keyword": "Dado "
});
formatter.match({
  "location": "CommonSteps.acessarSiteDoBlogDoAgi()"
});
formatter.result({
  "duration": 1834413625,
  "status": "passed"
});
formatter.scenario({
  "line": 25,
  "name": "[Pesquisa] - Realiza Pesquisa no Blog Agi preenchendo o campo de pesquisa com caracteres especiais",
  "description": "",
  "id": "valida-as-possibilidades-de-pesquisa-no-blog-do-agibank;[pesquisa]---realiza-pesquisa-no-blog-agi-preenchendo-o-campo-de-pesquisa-com-caracteres-especiais",
  "type": "scenario",
  "keyword": "Cenario",
  "tags": [
    {
      "line": 24,
      "name": "@pesquisa"
    }
  ]
});
formatter.step({
  "line": 26,
  "name": "realizo uma pesquisa por \"@$#%@%$#\"",
  "keyword": "Quando "
});
formatter.step({
  "line": 27,
  "name": "a pesquisa não deve retornar resultados",
  "keyword": "Entao "
});
formatter.match({
  "arguments": [
    {
      "val": "@$#%@%$#",
      "offset": 26
    }
  ],
  "location": "blogDoAgiHomeSteps.pesquisarNoBlog(String)"
});
formatter.result({
  "duration": 1430556084,
  "status": "passed"
});
formatter.match({
  "location": "blogDoAgiResultadoPesquisaSteps.validarQuePesquisaNaoRetornouResultados()"
});
formatter.result({
  "duration": 252120750,
  "status": "passed"
});
formatter.after({
  "duration": 184014833,
  "status": "passed"
});
});